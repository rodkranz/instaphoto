package main

import (
	"os"
	"runtime"

	"github.com/codegangsta/cli"

	"bitbucket.org/rodkranz/instagram/cmd"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

const APP_VER = "1.0.0"

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	setting.AppVer = APP_VER
}

func main() {
	app := cli.NewApp()
	app.Name = "instagram"
	app.Usage = "Instagram"
	app.Version = APP_VER
	app.Commands = []cli.Command{
		cmd.CmdWeb,
		cmd.CmdSync,
	}

	app.Flags = append(app.Flags, []cli.Flag{}...)
	app.Run(os.Args)
}
