var menu    = [
	{title: 'Home', href: '#'},
	{title: 'About', href: '#'},
];
var hashtag = window.location.hash || 'PokemonGo';

// ***************************************************************************
// Menu Bar
// ***************************************************************************
var MenuItem          = React.createClass({
	getInitialState: function () {
		return {
			title: '',
			href:  '',
		}
	},
	render:          function () {
		return (
			<div className="item">
				<a href={this.props.href}>
					{this.props.title}
				</a>
			</div>
		)
	}
});
var FormSearchHashtag = React.createClass({
	getInitialState:     function () {
		return {hashtag: this.props.hashtag}
	},
	handleHashtagChange: function (e) {
		this.setState({hashtag: e.target.value});
	},
	submitHashtagChange: function (e) {
		e.preventDefault();
		var hashtag = this.state.hashtag.trim();
		if (!hashtag) {
			return;
		}
		this.props.toggleDimmer('show');
		this.setState({hashtag: hashtag});
		this.props.onChangeHashtag(hashtag);
	},
	render:              function () {
		return (
			<div className="right item">
				<form className="ui action input" onSubmit={this.submitHashtagChange}>
					<input type="text"
					       placeholder="# HashTag..."
					       value={this.state.hashtag}
					       onChange={this.handleHashtagChange}
					/>

					<button className="ui button" type="submit">Search</button>
				</form>
			</div>
		)
	}
});
var NavBar            = React.createClass({
	getInitialState: function () {
		return {hashtag: this.props.hashtag};
	},
	render:          function () {
		var ItemList = this.props.menu.map(function (item) {
			return (
				<MenuItem title={item.title} href={item.href}/>
			)
		});
		return (
			<div className="ui menu pointing">
				<div className="ui container">
					{ItemList}

					<FormSearchHashtag
						hashtag={this.props.hashtag}
						toggleDimmer={this.props.toggleDimmer}
						onChangeHashtag={this.props.onChangeHashtag}/>
				</div>
			</div>
		)
	}
});
// ***************************************************************************


// ***************************************************************************
// Center Images
// ***************************************************************************
var HeaderTotal = React.createClass({
	componentDidMount: function () {
		$('.ui.dropdown').dropdown();
	},
	render:            function () {
		var hashtag = this.props.hashtag.replace("#", "");

		var labelStyle = {
			top: -45
		};

		var settingsStyle = {
			right:    7,
			position: "absolute",
			top:      7
		};

		return (
			<div id="head">

				<button className="ui icon button small" style={settingsStyle}>
					<i className="wrench icon"/>
				</button>

				<h2 className="ui dividing header">
					<i className="hashtag icon"/>
					<div className="content">
						{hashtag}
					</div>
				</h2>
				<a style={labelStyle} className="ui blue right ribbon label">
					Total: {this.props.total}
				</a>
			</div>
		)
	}
});
var Card        = React.createClass({
	onShowModal: function () {
		this.props.onOpenModal(this.props.image);
	},
	render:      function () {
		return (
			<div className="column">
				<div className="ui card link openModal centered" id={this.props.image.iid} onClick={this.onShowModal}>
					<div className="content">
						<div className="right floated meta">{this.props.image.iid}</div>
						<div className="floated meta">{this.props.image.id}</div>
					</div>
					<div className="image">
						<img src={this.props.image.thumbnail_src}/>
					</div>
					<div className="content">
                        <span className="right floated">
                          <i className="heart outline like icon"/>
	                        {this.props.image.likes} likes
                        </span>
					</div>
				</div>
			</div>
		)
	}
});
var CardList    = React.createClass({
	render: function () {
		var cardNodes = this.props.data.map(function (image) {
			return (
				<Card image={image} onOpenModal={this.props.onOpenModal}/>
			)
		}.bind(this));

		return (
			<div className="ui three column doubling grid container">
				{cardNodes}
			</div>
		)
	}
});
var BoxImages   = React.createClass({
	getInitialState:      function () {
		return {data: [], total: 0, hashtag: this.props.hashtag};
	},
	loadImagesFromServer: function () {
		if (!this.props.hashtag) {
			return;
		}

		var uri = this.props.url.replace("%hashtag%", this.props.hashtag).replace("#", "");
		$.ajax({
			url:      uri,
			dataType: 'json',
			success:  function (data) {
				var hashtag = data.hashtag || this.props.hashtag;
				this.setState({data: data.data, total: data.total, hashtag: hashtag});
				this.props.toggleDimmer('hide');
			}.bind(this),
			error:    function (xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	},
	componentDidMount:    function () {
		setTimeout(this.loadImagesFromServer, 100);
		setInterval(this.loadImagesFromServer, this.props.pollInterval);
	},
	renderBox:            function () {
		return (
			<div className="ui segment raised">
				<HeaderTotal hashtag={this.state.hashtag} total={this.state.total}/>
				<CardList data={this.state.data} onOpenModal={this.props.onOpenModal}/>
			</div>
		)
	},
	renderNothing:        function () {
		var message = "There is no images to show for **%s** !".replace("%s", this.props.hashtag);
		return (
			<MessageAlert title="No Images" message={message}/>
		)
	},
	render:               function () {
		return (this.state.data.length === 0
		) ? this.renderNothing() : this.renderBox();
	}
});
// ***************************************************************************

// ***************************************************************************
// Loader
// ***************************************************************************
var Dimmer       = React.createClass({
	render: function () {
		return (
			<div className="ui BoxImages dimmer">
				<div className="content">
					<div className="center">Loading...</div>
				</div>
			</div>
		)
	}
});
var MessageAlert = React.createClass({
	getInitialState: function () {
		return {title: "", message: ""}
	},
	rawMarkup:       function () {
		var md        = new Remarkable();
		var rawMarkup = md.render(this.props.message.toString());
		return {__html: rawMarkup};
	},
	render:          function () {
		var md = new Remarkable();
		return (
			<div className="ui negative message">
				<div className="header">
					{this.props.title}
				</div>
				<p dangerouslySetInnerHTML={this.rawMarkup()}/>
			</div>
		)
	}
});
var ModalShow    = React.createClass({
	componentDidMount: function () {
		$('.ui.modal').modal({
			approve:  '.positive, .approve, .ok',
			deny:     '.negative, .deny, .cancel',
			blurring: true
		});
	},
	render:            function () {
		var item = this.props.item || {};
		return (
			<div className="ui fullscreen modal long">
				<i className="close icon"/>
				<div className="header">
					{item.hashtag}
				</div>
				<div className="ui centered big content">
					<img className="ui centered image big" src={item.display_src}/>
					<div className="ui centered description">
						<p>{item.caption}</p>
					</div>
				</div>
				<div className="actions">
					<div className="ui button ok">OK</div>
				</div>
			</div>
		)
	}
});
// ***************************************************************************

// ***************************************************************************
// Application
// ***************************************************************************
var App = React.createClass({
	toggleDimmer:      function (action) {
		action = !!action && action || 'toggle';
		$('.ui.BoxImages.dimmer').dimmer(action);
	},
	getInitialState:   function () {
		return {hashtag: this.props.hashtag, item: null}
	},
	onChangeHashtag:   function (hashtag) {
		window.location.hash = hashtag;
		this.setState({hashtag: hashtag});
	},
	onOpenModal:       function (item) {
		this.setState({hashtag: this.props.hashtag, item: item});
		$('.ui.modal').modal('show');
	},
	componentDidMount: function () {
		this.toggleDimmer('show');
	},
	render:            function () {
		return (
			<div>
				<NavBar
					menu={this.props.menu}
					hashtag={this.state.hashtag}
					toggleDimmer={this.toggleDimmer}
					onChangeHashtag={this.onChangeHashtag}/>

				<div className="ui container ">
					<BoxImages hashtag={this.state.hashtag}
					           toggleDimmer={this.toggleDimmer}
					           url="/api/v1/images/search/?q=%hashtag%"
					           pollInterval={5000}
					           onOpenModal={this.onOpenModal}/>
				</div>

				<Dimmer />

				<ModalShow item={this.state.item}/>
			</div>
		)
	}
});

ReactDOM.render(
	<App menu={menu} hashtag={hashtag}/>,
	document.getElementById('content')
);
// ***************************************************************************