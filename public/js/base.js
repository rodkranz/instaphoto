(function (w) {
		'use strict';

		w.insta = {
			dinner:         dinner,
			openModalImage: openModalImage
		};


		function openModalImage(id) {
			var uri = '/api/v1/images/'.concat(id);
			var req = $.getJSON(uri);

			req.done(function (data) {

				$('#modelImage')
					.attr({
						'src': data.resource.display_src,
					});
//	                .css({
//						'width':  '300px',
//						'height': '300px'
//					});

				var titleHeader = '<i class="hashtag icon"></i>'.concat(data.resource.hashtag);

				$('#modelHeader').html(titleHeader);
				$('#modelDescription').html(data.resource.caption);

				$('.ui.modal')
					.modal('show')
			});


			req.fail(function (err) {
				console.error(err);
			});
		}

		function dinner() {
			var elem = $('.ui.page.dimmer');

			if (elem.hasClass('active')) {
				elem.removeClass('active');
			} else {
				elem.addClass('active');
			}
		}

	}
)(window);


$(function () {
	$('.ui.modal.image').modal({
		closable: true,
		approve:  '.positive, .approve, .ok',
		deny:     '.negative, .deny, .cancel'
	});

	$('.card.link.openModal').click(function () {
		window.insta.openModalImage($(this).attr('id'));
	})
});

//http://vnjs.net/www/project/freewall/