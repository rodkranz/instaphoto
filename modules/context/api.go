package context

import (
	"gopkg.in/macaron.v1"

	"bitbucket.org/rodkranz/instagram/modules/log"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

type APIContext struct {
	*Context
}

// Error responses error message to client with given message.
// If status is 500, also it prints error to log.
func (ctx *APIContext) Error(status int, title string, obj interface{}) {
	var message string
	if err, ok := obj.(error); ok {
		obj = err.Error()
	}

	if status == 500 {
		log.Error(4, "%s: %s", title, message)
	}

	ctx.JSON(status, map[string]interface{}{
		"message":  title,
		"status":   status,
		"resource": obj,
	})
}

func (ctx *APIContext) Render(status int, title string, obj interface{}) {
	ctx.JSON(status, map[string]interface{}{
		"message":  title,
		"status":   status,
		"resource": obj,
	})
}

func APIContexter() macaron.Handler {
	return func(c *Context) {
		ctx := &APIContext{
			Context: c,
		}

		ctx.Resp.Header().Set("Content-Type", "application/json; charset=UTF-8")
		ctx.Resp.Header().Set("Server", "GoLang v.1.6")
		ctx.Resp.Header().Set("X-Resource", "resource")
		ctx.Resp.Header().Set("CRSF", ctx.Data["CsrfToken"].(string))

		if setting.AllowCrossDomain {
			ctx.Resp.Header().Set("Access-Control-Allow-Origin", ctx.Req.Host)
			ctx.Resp.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			ctx.Resp.Header().Set("Access-Control-Max-Age", "1000")
			ctx.Resp.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With")
		}

		c.Map(ctx)
	}
}
