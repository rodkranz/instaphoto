package validate

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
)

type RegisterForm struct {
	FullName      string `binding:"Required;MaxSize(50)"`
	UserName      string `binding:"Required;AlphaDashDot;MaxSize(200)"`
	Password      string `binding:"Required;MaxSize(255)"`
	Retype        string
}

func (r *RegisterForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, r, ctx.Locale)
}

type SignInForm struct {
	UserName string `binding:"Required;MaxSize(254)"`
	Password string `binding:"Required;MaxSize(255)"`
	Remember bool
}

func (s *SignInForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, s, ctx.Locale)
}

type ProfileForm struct {
	FullName      string `binding:"Required;MaxSize(50)"`
	UserName      string `binding:"Required;AlphaDashDot;MaxSize(200)"`
	Password      string `binding:"MaxSize(255)"`
	Retype        string
}

func (p *ProfileForm) Validate(ctx *macaron.Context, errs binding.Errors) binding.Errors {
	return validate(errs, ctx.Data, p, ctx.Locale)
}
