package template

import (
	"fmt"
	"html/template"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/rodkranz/instagram/modules/base"
	"bitbucket.org/rodkranz/instagram/modules/markdown"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

func NewFuncMap() []template.FuncMap {
	return []template.FuncMap{map[string]interface{}{
		"GoVer": func() string {
			return strings.Title(runtime.Version())
		},
		"UseHTTPS": func() bool {
			return strings.HasPrefix(setting.AppUrl, "https")
		},
		"AppName": func() string {
			return setting.AppName
		},
		"AppDesc": func() string {
			return setting.AppDesc
		},
		"AppSubUrl": func() string {
			return setting.AppSubUrl
		},
		"AppUrl": func() string {
			return setting.AppUrl
		},
		"AppVer": func() string {
			return setting.AppVer
		},
		"AppDomain": func() string {
			return setting.Domain
		},
		"LoadTimes": func(startTime time.Time) string {
			return fmt.Sprint(time.Since(startTime).Nanoseconds()/1e6) + "ms"
		},
		"ShortSha":     base.ShortSha,
		"MD5":          base.EncodeMD5,
		"TimeSince":    base.TimeSince,
		"RawTimeSince": base.RawTimeSince,
		"FileSize":     base.FileSize,
		"Subtract":     base.Subtract,
		"Str2html":     Str2html,
	}}
}

func Safe(raw string) template.HTML {
	return template.HTML(raw)
}

func Range(l int) []int {
	return make([]int, l)
}

func Str2html(raw string) template.HTML {
	return template.HTML(markdown.Sanitizer.Sanitize(raw))
}
