package setting

import (
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/Unknwon/com"
	"github.com/go-macaron/session"
	"gopkg.in/ini.v1"

	_ "github.com/go-macaron/cache/memcache"
	_ "github.com/go-macaron/cache/redis"
	_ "github.com/go-macaron/session/redis"

	"bitbucket.org/rodkranz/instagram/modules/bindata"
	"bitbucket.org/rodkranz/instagram/modules/log"
)

type Scheme string

const (
	HTTP Scheme = "http"
	HTTPS Scheme = "https"
	FCGI Scheme = "fcgi"
)

var (
	// Build information
	BuildTime string
	BuildGitHash string

	// App settings
	AppVer string
	AppName string
	AppDesc string
	AppUrl string
	AppSubUrl string
	AppSubUrlDepth int // Number of slashes
	AppPath string
	AppDataPath string

	// I18n settings
	Langs, Names []string
	dateLangs    map[string]string

	// Server settings
	Protocol Scheme
	Domain string
	HttpAddr, HttpPort string
	LocalURL string
	OfflineMode bool
	DisableRouterLog bool
	CertFile, KeyFile string
	StaticRootPath string
	EnableGzip bool

	// sync
	Instagram struct {
		Url string
	}

	// Session settings
	SessionConfig session.Options
	CSRFCookieName = "_csrf"

	// Security settings
	SecretKey string
	LogInRememberDays int
	CookieUserName string
	CookieRememberName string

	// Time settings
	TimeFormat string

	// UI
	ExplorePagingNum int

	// Log settings
	LogRootPath string
	LogModes    []string
	LogConfigs  []string

	// Global setting objects
	Cfg          *ini.File
	CustomPath string // Custom directory path
	CustomConf string
	ProdMode bool
	RunUser string
	IsWindows bool
	HasRobotsTxt bool

	// Database settings
	UseSQLite3 bool
	UseMySQL bool
	UsePostgreSQL bool
	UseTiDB bool

	// Markdown sttings
	Markdown struct {
		EnableHardLineBreak bool
		CustomURLSchemes    []string `ini:"CUSTOM_URL_SCHEMES"`
	}

	// Cache settings
	CacheAdapter string
	CacheInternal int
	CacheConn string

	// Api
	AllowCrossDomain bool
)

// execPath returns the executable path.
func execPath() (string, error) {
	file, err := exec.LookPath(os.Args[0])
	if err != nil {
		return "", err
	}
	return filepath.Abs(file)
}

func init() {
	IsWindows = runtime.GOOS == "windows"
	log.NewLogger(0, "console", `{"level": 0}`)

	var err error
	if AppPath, err = execPath(); err != nil {
		log.Fatal(4, "fail to get app path: %v\n", err)
	}

	// Note: we don't use path.Dir here because it does not handle case
	//      which path starts with two "/" in Windows: "//psf/Home/..."
	AppPath = strings.Replace(AppPath, "\\", "/", -1)
}

// WorkDir returns absolute path of work directory.
func WorkDir() (string, error) {
	wd := os.Getenv("HD_WORK_DIR")
	if len(wd) > 0 {
		return wd, nil
	}

	i := strings.LastIndex(AppPath, "/")
	if i == -1 {
		return AppPath, nil
	}
	return AppPath[:i], nil
}

func forcePathSeparator(path string) {
	if strings.Contains(path, "\\") {
		log.Fatal(4, "Do not use '\\' or '\\\\' in paths, instead, please use '/' in all places")
	}
}

// NewContext initializes configuration context.
// NOTE: do not print any log except error.
func NewContext() {
	workDir, err := WorkDir()
	if err != nil {
		log.Fatal(4, "Fail to get work directory: %v", err)
	}

	Cfg, err = ini.Load(bindata.MustAsset("conf/app.ini"))
	if err != nil {
		log.Fatal(4, "Fail to parse 'conf/app.ini': %v", err)
	}

	CustomPath = os.Getenv("HD_CUSTOM")
	if len(CustomPath) == 0 {
		CustomPath = workDir + "/custom"
	}

	if len(CustomConf) == 0 {
		CustomConf = CustomPath + "/conf/app.ini"
	}

	if com.IsFile(CustomConf) {
		if err = Cfg.Append(CustomConf); err != nil {
			log.Fatal(4, "Fail to load custom conf '%s': %v", CustomConf, err)
		}
	} else {
		log.Warn("Custom config '%s' not found, ignore this if you're running first time", CustomConf)
	}
	Cfg.NameMapper = ini.AllCapsUnderscore

	homeDir, err := com.HomeDir()
	if err != nil {
		log.Fatal(4, "Fail to get home directory: %v", err)
	}
	homeDir = strings.Replace(homeDir, "\\", "/", -1)

	LogRootPath = Cfg.Section("log").Key("ROOT_PATH").MustString(path.Join(workDir, "log"))
	forcePathSeparator(LogRootPath)

	sec := Cfg.Section("server")
	AppName = Cfg.Section("").Key("APP_NAME").MustString("HD: Instagram Service")
	AppDesc = Cfg.Section("").Key("APP_DESCRIPTION").MustString("Instagram App Description")
	AppUrl = sec.Key("ROOT_URL").MustString("http://localhost:3000/")
	if AppUrl[len(AppUrl) - 1] != '/' {
		AppUrl += "/"
	}

	// Check if has app suburl.
	url, err := url.Parse(AppUrl)
	if err != nil {
		log.Fatal(4, "Invalid ROOT_URL '%s': %s", AppUrl, err)
	}

	// Suburl should start with '/' and end without '/', such as '/{subpath}'.
	AppSubUrl = strings.TrimSuffix(url.Path, "/")
	AppSubUrlDepth = strings.Count(AppSubUrl, "/")

	Protocol = HTTP
	if sec.Key("PROTOCOL").String() == "https" {
		Protocol = HTTPS
		CertFile = sec.Key("CERT_FILE").String()
		KeyFile = sec.Key("KEY_FILE").String()
	} else if sec.Key("PROTOCOL").String() == "fcgi" {
		Protocol = FCGI
	}

	Domain = sec.Key("DOMAIN").MustString("localhost")
	HttpAddr = sec.Key("HTTP_ADDR").MustString("0.0.0.0")
	HttpPort = sec.Key("HTTP_PORT").MustString("3000")
	LocalURL = sec.Key("LOCAL_ROOT_URL").MustString("http://localhost:" + HttpPort + "/")
	OfflineMode = sec.Key("OFFLINE_MODE").MustBool()
	DisableRouterLog = sec.Key("DISABLE_ROUTER_LOG").MustBool()
	StaticRootPath = sec.Key("STATIC_ROOT_PATH").MustString(workDir)
	AppDataPath = sec.Key("APP_DATA_PATH").MustString("data")
	EnableGzip = sec.Key("ENABLE_GZIP").MustBool()

	HasRobotsTxt = com.IsFile(path.Join(CustomPath, "robots.txt"))

	AllowCrossDomain = Cfg.Section("API").Key("allowCrossDomain").MustBool(true)

	Instagram.Url = Cfg.Section("instagram").Key("uri").MustString("https://www.instagram.com/explore/tags/%s/")

	ExplorePagingNum = Cfg.Section("ui").Key("EXPLORE_PAGING_NUM").MustInt(20)

	Langs = Cfg.Section("i18n").Key("LANGS").Strings(",")
	Names = Cfg.Section("i18n").Key("NAMES").Strings(",")
	dateLangs = Cfg.Section("i18n.datelang").KeysHash()
}
