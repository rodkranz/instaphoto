package instagram

import (
	"encoding/json"

	"bitbucket.org/rodkranz/instagram/models"
)

type Skeleton struct {
	CountryCode  string    `json:"country_code"`
	LanguageCode string    `json:"language_code"`
	Platform     string    `json:"platform"`
	Hostname     string    `json:"hostname"`
	Data         EntryData `json:"entry_data"`
}

type EntryData struct {
	TagPages []TagPage `json:"TagPage"`
}

type TagPage struct {
	Tags Tag `json:"tag"`
}

type Tag struct {
	Name     string  `json:"name"`
	Medias   Media   `json:"media"`
	TopPosts TopPost `json:"top_posts"`
}

type Media struct {
	Count int         `json:"count"`
	Nodes []NodeMedia `json:"nodes"`
}

type TopPost struct {
	Nodes []NodeMedia `json:"nodes"`
}

type NodeMedia struct {
	Used         bool   `json:"used"`
	Caption      string `json:"caption"`
	ThumbnailSrc string `json:"thumbnail_src"`
	Id           string `json:"id"`
	DisplaySrc   string `json:"display_src"`
	Likes        struct {
		             Count int  `json:"count"`
	             } `json:"likes"`
	Dimensions   struct {
		             Width  int `json:"width"`
		             Height int `json:"height"`
	             }  `json:"dimensions"`
}

func (n *NodeMedia) SetUsed(s string) {
	if s == "y" {
		n.Used = true
	} else {
		n.Used = false
	}
}
func (n *NodeMedia) ToModel() *models.Image {
	photo := new(models.Image)

	photo.IId = n.Id
	photo.Caption = n.Caption
	photo.DisplaySrc = n.DisplaySrc
	photo.ThumbnailSrc = n.ThumbnailSrc
	photo.Likes = n.Likes.Count
	photo.Height = n.Dimensions.Height
	photo.Width = n.Dimensions.Width
	photo.Used = false

	return photo
}

func (m *Skeleton) Parse(s string) error {
	return json.Unmarshal([]byte(s), &m)
}