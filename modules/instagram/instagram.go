package instagram

import (
	"fmt"
	"regexp"
	"net/http"
	"io/ioutil"

	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

func ImportPhotos(hashtag string) (int64, int64, error) {
	skeleton := &Skeleton{}

	uri := fmt.Sprintf(setting.Instagram.Url, hashtag)
	res, err := downloadHTML(uri)
	if err != nil {
		return 0, 0, err
	}

	jsonData, err := parseHTML(res)
	if err != nil {
		return 0, 0, err
	}

	if err := skeleton.Parse(jsonData); err != nil {
		return 0, 0, err
	}

	var totalNew, totalEdit int64
	totalNew, totalEdit = 0, 0
	for _, t := range skeleton.Data.TagPages {
		for _, n := range t.Tags.Medias.Nodes {
			model := n.ToModel()
			model.HashTag = hashtag

			err := models.UpdateImageByIId(n.Id, model)
			if models.IsErrImageNotExist(err) {
				models.CreateImage(model)
				totalNew++
			} else {
				totalEdit++
			}

		}
		for _, n := range t.Tags.TopPosts.Nodes {
			model := n.ToModel()
			model.HashTag = hashtag

			err := models.UpdateImageByIId(n.Id, model)
			if models.IsErrImageNotExist(err) {
				models.CreateImage(model)
				totalNew++
			} else {
				totalEdit++
			}
		}
	}

	var info *models.Information
	if info, err = models.GetInformationByHashtag(hashtag); models.IsErrInformationNotExist(err) {
		info = &models.Information{HashTag: hashtag, Total: totalNew}
		models.CreateInformation(info)
	} else{
		info.Increment()
		models.UpdateInformation(info)
	}

	return totalNew, totalEdit, nil;
}

func downloadHTML(uri string) ([]byte, error) {
	res, err := http.Get(uri)
	if err != nil {
		return []byte(""), fmt.Errorf("Get Error: %s", err)
	}
	defer res.Body.Close()

	return ioutil.ReadAll(res.Body)
}

func parseHTML(html []byte) (string, error) {
	reg, err := regexp.Compile("window._sharedData =(.*);</script>")
	if err != nil {
		return "", fmt.Errorf("Regex Error: %s", err)
	}

	found := reg.FindAllStringSubmatch(string(html), -1)
	if len(found) == 0 {
		return "", fmt.Errorf("Found Html Error: %s", err)
	}

	if len(found) == 0 {
		return "", fmt.Errorf("Not Found: %s", err)
	}

	if len(found[0]) == 0 {
		return "", fmt.Errorf("Not Found: %s", err)
	}

	resultFound := string(found[0][0])

	return resultFound[len("window._sharedData ="):len(resultFound) - len(";</script>")], nil
}