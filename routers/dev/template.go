package dev

import (
	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/base"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

func TemplatePreview(ctx *context.Context) {
	ctx.Data["User"] = models.User{FullName: "Unknown"}
	ctx.Data["AppName"] = setting.AppName
	ctx.Data["AppVer"] = setting.AppVer
	ctx.Data["AppUrl"] = setting.AppUrl
	ctx.Data["Code"] = "2014031910370000009fff6782aadb2162b4a997acb69d4400888e0b9274657374"
	ctx.Data["ActiveCodeLives"] = setting.Service.ActiveCodeLives / 60
	ctx.Data["CurDbValue"] = ""

	ctx.HTML(200, base.TplName(ctx.Params("*")))
}
