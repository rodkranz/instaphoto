package common

import (
	"net/http"
	"bitbucket.org/rodkranz/instagram/modules/context"
)

func FourOhFour(ctx *context.APIContext) {
	ctx.JSON(http.StatusNotFound, map[string]interface{}{
		"message":  "Page Not Found",
		"status":   http.StatusNotFound,
		"resource": nil,
	})
}
