package convert

import (
	"bitbucket.org/rodkranz/instagram/models"
	api "bitbucket.org/rodkranz/instagram/routers/api/client"
)

func ToInformation(info *models.Information) *api.Information {
	if info == nil {
		return nil
	}

	return &api.Information{
		Id: info.Id,
		HashTag: info.HashTag,
		Total: info.Total,
	}
}

func ToInformations(infos []*models.Information) []*api.Information {
	list := make([]*api.Information, len(infos))

	for key, img := range infos {
		list[key] = ToInformation(img)
	}

	return list
}

func ToInformationWithImages(info *models.Information) *api.InformationWithImage {
	if info == nil {
		return nil
	}

	images, _ := info.GetImages()

	return &api.InformationWithImage{
		Id: info.Id,
		HashTag: info.HashTag,
		Total: info.Total,
		Images: ToImages(images),
	}
}

