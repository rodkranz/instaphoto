package convert

import (
	api "bitbucket.org/rodkranz/instagram/routers/api/client"

	"bitbucket.org/rodkranz/instagram/models"
)

func ToUser(u *models.User) *api.User {
	if u == nil {
		return nil
	}

	return &api.User{
		ID:       u.Id,
		FullName: u.FullName,
		UserName: u.UserName,
	}
}

func ToUsers(users []*models.User) []*api.User {
	list := make([]*api.User, len(users))

	for i, u := range users {
		list[i] = ToUser(u)
	}

	return list
}

func ToUserAuth(u *models.User, t *models.AccessToken) *api.UserLogin {
	if u == nil {
		return nil
	}

	return &api.UserLogin{
		ID:       u.Id,
		UserName: u.UserName,
		FullName: u.FullName,
		Token:    t.Sha1,
	}
}
