package convert

import (
	"bitbucket.org/rodkranz/instagram/models"
	api "bitbucket.org/rodkranz/instagram/routers/api/client"
)

func ToImage(img *models.Image) *api.Image {
	if img == nil {
		return nil
	}

	return &api.Image{
		ID: img.Id,
		IId: img.IId,
		Caption: img.Caption,
		DisplaySrc: img.DisplaySrc,
		ThumbnailSrc: img.ThumbnailSrc,
		HashTag: img.HashTag,
		Likes: img.Likes,
		Width: img.Width,
		Height: img.Height,
		Used: img.Used,
	}
}

func ToImages(images []*models.Image) []*api.Image {
	list := make([]*api.Image, len(images))

	for key, img := range images {
		list[key] = ToImage(img)
	}

	return list
}

