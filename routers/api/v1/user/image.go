package user

import (
	"strings"
	"net/http"
	"github.com/Unknwon/com"
	
	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/modules/instagram"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/convert"
)

func SearchImages(ctx *context.APIContext) {
	hashTag := ctx.Query("q")
	
	if strings.HasPrefix(hashTag, "#") {
		hashTag = hashTag[1:]
	}
	
	instagram.ImportPhotos(hashTag)
	
	opts := &models.SearchImageOptions{
		Keyword:  hashTag,
		PageSize: com.StrTo(ctx.Query("limit")).MustInt(),
	}
	
	if opts.PageSize == 0 {
		opts.PageSize = 33
	}
	
	images, _, err := models.SearchImageByHashtag(opts)
	if err != nil {
		ctx.JSON(500, map[string]interface{}{
			"ok":    false,
			"error": err.Error(),
		})
		return
	}
	
	ctx.JSON(200, map[string]interface{}{
		"ok":   true,
		"total": models.CountImageHashtag(hashTag),
		"hashtag": hashTag,
		"data": convert.ToImages(images),
	})
}

func GetImage(ctx *context.APIContext) {
	i, err := models.GetImageByIId(ctx.Params(":iid"))
	if err != nil {
		ctx.Error(404, "Image not found", err)
		return
	}
	
	ctx.Render(http.StatusOK, "Image found", convert.ToImage(i))
}
