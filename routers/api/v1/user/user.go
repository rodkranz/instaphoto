package user

import (
	"net/http"
	
	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/log"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/modules/validate"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/convert"
)

func GetUserByParams(ctx *context.APIContext, name string) *models.User {
	user, err := models.GetUserByName(ctx.Params(name))
	if err != nil {
		if models.IsErrUserNotExist(err) {
			ctx.Error(http.StatusNotFound, "User not found", err)
		} else {
			ctx.Error(http.StatusInternalServerError, "Get User By Name", err)
		}
		return nil
	}
	return user
}

/**
 * GetUserByParams returns user whose name is presented in URL paramenter.
 */
func GetUserByParamsName(ctx *context.APIContext) *models.User {
	return GetUserByParams(ctx, ":username")
}


/**
 * CurrentUser return the current user by token
 */
func CurrentUser(ctx *context.APIContext) {
	u := ctx.User
	
	tokens, err := models.ListAccessTokens(u.Id)
	if err != nil {
		ctx.Error(http.StatusInternalServerError, "GetUserByName", err)
		return
	}
	
	if len(tokens) > 0 {
		ctx.Render(http.StatusOK, "Current user", convert.ToUserAuth(u, tokens[0]))
		return
	}
	
	ctx.Render(http.StatusOK, "Current user", convert.ToUser(u))
}


/**
 * UpdateUserProfile: Update profile of user logged
 */
func UpdateUserProfile(ctx *context.APIContext, form validate.ProfileForm) {
	u := ctx.User
	
	if len(form.Password) > 0 {
		u.Passwd = form.Password
		u.Salt = models.GetUserSalt()
		u.EncodePasswd()
	}
	
	u.UserName = form.UserName
	u.FullName = form.FullName
	
	if err := models.UpdateUser(u); err != nil {
		if models.IsErrUserAlreadyExist(err) {
			ctx.Error(422, "", err)
		} else {
			ctx.Error(http.StatusInternalServerError, "Update User", err)
		}
		return
	}
	
	log.Trace("Account profile updated: %s", u.UserName)
	
	ctx.Render(http.StatusOK, "User has been updated successfully", convert.ToUser(u))
}
