package user

import (
	"net/http"

	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/log"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/modules/validate"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/convert"
)

func SignUpPost(ctx *context.APIContext, form validate.RegisterForm) {
	if form.Password != form.Retype {
		ctx.Error(http.StatusBadRequest, "The user has been not registred successfully",
			map[string]interface{}{
				"password": "password doesnt match",
			})
		return
	}
	
	u := &models.User{
		FullName:      form.FullName,
		UserName:      form.UserName,
		Passwd:        form.Password,
	}
	
	var err error
	if err = models.CreateUser(u); err != nil {
		switch {
		case models.IsErrUserAlreadyExist(err):
			ctx.Error(http.StatusBadRequest, "form.user_been_taken", err)
		default:
			ctx.Handle(http.StatusInternalServerError, "CreateUser", err)
		}
		return
	}
	log.Trace("Account created: %s", u.UserName)
	
	ctx.Render(http.StatusCreated, "The user has been registered successfully", convert.ToUser(u))
}

func SignInPost(ctx *context.APIContext, form validate.SignInForm) {

	u, t, err := models.UserSignIn(form.UserName, form.Password)
	if err != nil {
		if models.IsErrUserNotExist(err) {
			ctx.Error(http.StatusBadRequest, "Username or password is incorrect", err)
		} else {
			ctx.Handle(http.StatusInternalServerError, "User sign in", err)
		}
		return
	}

	ctx.Render(http.StatusOK, "The user has been logged successfully", convert.ToUserAuth(u, t))
}

