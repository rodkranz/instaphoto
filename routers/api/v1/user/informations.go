package user

import (
	"net/http"

	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/convert"
)

func GetInformations(ctx *context.APIContext) {
	i, err := models.GetInformations()
	if err != nil {
		ctx.Error(500, "Error to find Informations", err)
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"message":  "List of informations found",
		"status":   http.StatusOK,
		"total":    models.CountInformation(),
		"resource": convert.ToInformations(i),
	})
}

func GetInformationByHashtag(ctx *context.APIContext) {
	info, err := models.GetInformationByHashtag(ctx.Params("hashtag"))
	if err != nil {
		ctx.Error(500, "Error to find Informations", err)
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"message":   "Information",
		"status":    http.StatusOK,
		"resource":  convert.ToInformation(info),
	})
}

func GetImagesFromInformation(ctx *context.APIContext) {
	info, err := models.GetInformationByHashtag(ctx.Params("hashtag"))
	if err != nil {
		ctx.Error(500, "Error to find Informations", err)
		return
	}

	ctx.JSON(http.StatusOK, map[string]interface{}{
		"message":   "Information with images",
		"status":    http.StatusOK,
		"resource":  convert.ToInformationWithImages(info),
	})

}