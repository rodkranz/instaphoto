package v1

import (
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"

	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/modules/validate"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/common"
	"bitbucket.org/rodkranz/instagram/routers/api/v1/user"
)

/**
 * Contexter middleware already checks token for user sign in process.
 */
func ReqToken() macaron.Handler {
	return func(ctx *context.APIContext) {
		if !ctx.IsSigned {
			ctx.Error(401, "Unauthorized access", nil)
			return
		}
	}
}

func RegisterRoutes(m *macaron.Macaron) {
	bind := binding.Bind

	//Api v1
	m.Group("/v1", func() {
		// Images
		m.Group("/images", func() {
			m.Get("/search", user.SearchImages)
			m.Get("/:iid",   user.GetImage)
		})

		m.Group("/informations", func() {
			m.Get("", user.GetInformations)
			m.Get("/:hashtag", user.GetInformationByHashtag)
			m.Get("/:hashtag/images", user.GetImagesFromInformation)
		})


		// Users without token
		m.Group("/users", func() {
			m.Post("/sign_in", bind(validate.SignInForm{}), user.SignInPost)
			m.Post("/sign_up", bind(validate.RegisterForm{}), user.SignUpPost)
		})

		// Users with token
		m.Group("/users", func() {
			m.Get("/current", user.CurrentUser)
			m.Put("/profile", bind(validate.ProfileForm{}), user.UpdateUserProfile)
		}, ReqToken())

		// Four oh four page
		m.Any("/*", common.FourOhFour)

	}, context.APIContexter())
}
