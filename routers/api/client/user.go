package client

type User struct {
	ID       int64  `json:"id"`
	FullName string `json:"full_name"`
	UserName string `json:"username"`
}

type UserLogin struct {
	ID       int64  `json:"id"`
	UserName string `json:"username"`
	FullName string `json:"full_name"`
	Token    string `json:"token"`
}
