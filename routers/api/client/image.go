package client

type Image struct {
	ID           int64  `json:"id"`
	IId          string `json:"iid"`
	Caption      string `json:"caption"`
	DisplaySrc   string `json:"display_src"`
	ThumbnailSrc string `json:"thumbnail_src"`
	HashTag      string `json:"hashtag"`
	Likes        int    `json:"likes"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Used         bool   `json:"used"`
}