package client


type Information struct {
	Id           int64    `json:"id"`
	HashTag      string   `json:"hashtag"`
	Total        int64    `json:"total"`
}

type InformationWithImage struct {
	Id           int64    `json:"id"`
	HashTag      string   `json:"hashtag"`
	Total        int64    `json:"total"`
	Images       []*Image `json:"images"`
}
