package routers

import (
	"github.com/Unknwon/paginater"

	"bitbucket.org/rodkranz/instagram/modules/base"
	"bitbucket.org/rodkranz/instagram/modules/context"
	"bitbucket.org/rodkranz/instagram/models"
)

const (
	HOME base.TplName = "home"
	EXPLORE_IMAGES base.TplName = "images"
)


type ImagesSearchOptions struct {
	Counter  func() int64
	Ranger   func(int, int) ([]*models.Image, error)
	PageSize int
	OrderBy  string
	TplName  base.TplName
}

func RenderImageSearch(ctx *context.Context, opts *ImagesSearchOptions) {
	page := ctx.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		images []*models.Image
		count int64
		err   error
	)

	keyword := ctx.Query("q")
	if len(keyword) == 0 {
		images, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			ctx.Handle(500, "opts.Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		images, count, err = models.SearchImageByHashtag(&models.SearchImageOptions{
			Keyword:  keyword,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})
		if err != nil {
			ctx.Handle(500, "SearchImageByHashtag", err)
			return
		}
	}

	ctx.Data["Keyword"] = keyword
	ctx.Data["Total"] = count
	ctx.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	ctx.Data["Images"] = images

	ctx.HTML(200, opts.TplName)
}

func Home(ctx *context.Context) {
	infos, _ := models.GetInformations()

	ctx.Data["Categories"] = infos
	ctx.HTML(200, HOME)
}

func NotFound(ctx *context.Context) {
	ctx.Data["Title"] = "Page Not Found"
	ctx.Handle(404, "home.NotFound", nil)
}
