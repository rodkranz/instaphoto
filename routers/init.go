package routers

import (
	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/log"
	"bitbucket.org/rodkranz/instagram/modules/markdown"
	"bitbucket.org/rodkranz/instagram/modules/setting"
)

func NewServices() {
	setting.NewServices()
}

func GlobalInit() {
	setting.NewContext()

	log.Trace("Custom path: %s", setting.CustomPath)
	log.Trace("Log path: %s", setting.LogRootPath)

	models.LoadConfigs()
	NewServices()

	if err := models.NewEngine(); err != nil {
		log.Fatal(4, "Fail to initialize ORM engine: %v", err)
	}
	models.HasEngine = true

	// Build Sanitizer
	markdown.BuildSanitizer()
}
