package cmd

import (
	"strings"
	"errors"

	"github.com/codegangsta/cli"

	"bitbucket.org/rodkranz/instagram/models"
	"bitbucket.org/rodkranz/instagram/modules/setting"
	"bitbucket.org/rodkranz/instagram/modules/instagram"
	"bitbucket.org/rodkranz/instagram/modules/log"
	"time"
)

var CmdSync = cli.Command{
	Name:  "sync",
	Usage: "Import data from instagram",
	Description: `Import images from instagram site to DB`,
	Action: runSync,
	Flags: []cli.Flag{
		stringFlag("hashtag, hash", "yoga", "hashtag"),
	},
}

func runSync(ctx *cli.Context) error {
	hashtag := ctx.String("hashtag")

	if len(hashtag) == 0 {
		return errors.New("Missing required --hashtag parameter")
	}

	if strings.HasPrefix(hashtag, "#") {
		hashtag = hashtag[1:]
	}

	setting.NewContext()
	models.LoadConfigs()
	models.SetEngine()

	log.Info("Importing images [%s]", hashtag)
	totalNew, totalEdit, err := instagram.ImportPhotos(hashtag)
	log.Info("The photos has been imported [%v] new and [%v] edited with hashtag [#%v]", totalNew, totalEdit, hashtag)

	if err != nil {
		return err
	}

	time.Sleep(1 * time.Microsecond)
	return nil
}
