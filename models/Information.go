package models

import (
	"time"
	"github.com/go-xorm/xorm"
)


/**
 * Information model
 */
type Information struct {
	Id         int64

	HashTag     string `xorm:"UNIQUE INDEX NOT NULL"`
	Total       int64

	Created      time.Time `xorm:"-"`
	CreatedUnix  int64

	UsedTime      time.Time `xorm:"-"`
	UsedTimeUnix  int64
}

func (i *Information) BeforeInsert() {
	i.CreatedUnix = time.Now().UTC().Unix()
}

func (i *Information) BeforeUpdate() {
	i.UsedTimeUnix = time.Now().UTC().Unix()
}

func (i *Information) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		i.Created = time.Unix(i.CreatedUnix, 0).Local()
	case "used_time_unix":
		i.UsedTime = time.Unix(i.UsedTimeUnix, 0).Local()
	}
}

func (i *Information) Increment() {
	i.Total = CountImageHashtag(i.HashTag);
}

func (i *Information) IsNew() bool {
	return i.Id == 0
}

func (i *Information) GetImages() ([]*Image, error) {
	images := make([]*Image, 0, CountImageHashtag(i.HashTag))
	return images, x.Where("hash_tag=?", i.HashTag).Desc("id").Find(&images)
}

func (i *Information) CountImages() (int64) {
	count, _ := x.Where("hash_tag=?", i.HashTag).Count(new(Image))
	return count
}

func GetInformationByHashtag(hashTag string) (*Information, error) {
	t := &Information{HashTag: hashTag}
	has, err := x.Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrInformationNotExist{hashTag}
	}
	return t, nil
}


func IsInformationExit(uid int64, hashtag string) (bool, error) {
	if len(hashtag) == 0 {
		return false, nil
	}

	return x.Where("id!=?", uid).Get(&Information{HashTag: hashtag})
}

func CreateInformation(i *Information) error {
	return createInformation(x, i)
}

func createInformation(e Engine, i *Information) (err error) {
	isExist, err := IsInformationExit(0, i.HashTag)
	if err != nil {
		return err
	} else if isExist {
		return ErrImageNotExist{i.HashTag}
	}

	_, err = e.Insert(i);
	return err
}


func UpdateInformation(i *Information) error {
	return updateInformation(x, i)
}

func updateInformation(e Engine, i *Information) error {
	_, err := e.Id(i.Id).AllCols().Update(i)
	return err
}

func CountInformation() int64 {
	return countInformation(x)
}

func countInformation(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(Information))
	return count
}


func GetInformations() ([]*Information, error) {
	information := make([]*Information, 0, CountInformation())
	return information, x.Desc("id").Find(&information)
}


