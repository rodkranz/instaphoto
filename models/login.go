package models

import "strings"

func UserSignIn(uname, passwd string) (*User, *AccessToken, error) {
	u := &User{UserName: strings.ToLower(uname)}

	userExists, err := x.Get(u)
	if err != nil {
		return nil, nil, err
	}

	if userExists {
		if !u.ValidatePassword(passwd) {
			return nil, nil, ErrPasswordInvalid{}
		}

		at := &AccessToken{
			UID:  u.Id,
			Name: "Api",
		}

		if err = NewAccessToken(at); err != nil {
			return nil, nil, err
		}

		return u, at, nil
	}
	return nil, nil, ErrUserNotExist{u.Id, uname}
}
