package models

import (
	"time"
	"fmt"
	"github.com/go-xorm/xorm"
	"bitbucket.org/rodkranz/instagram/modules/setting"
	"bitbucket.org/rodkranz/instagram/modules/log"
)

/**
 * Image model
 */
type Image struct {
	Id           int64

	IId          string `xorm:"UNIQUE INDEX NOT NULL"`
	Caption      string `xorm:"NOT NULL"`
	DisplaySrc   string `xorm:"NOT NULL"`
	ThumbnailSrc string `xorm:"NOT NULL"`
	HashTag      string `xorm:"NOT NULL"`
	Likes        int    `xorm:"NOT NULL DEFAULT 0"`

	Width        int    `xorm:"-"`
	Height       int    `xorm:"-"`

	Used         bool   `xorm:"NOT NULL DEFAULT false"`

	Created      time.Time `xorm:"-"`
	CreatedUnix  int64

	Updated      time.Time `xorm:"-"`
	UpdatedUnix  int64
}

func (i *Image) BeforeInsert() {
	i.CreatedUnix = time.Now().UTC().Unix()
}

func (i *Image) BeforeUpdate() {
	i.UpdatedUnix = time.Now().UTC().Unix()
}

func (i *Image) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "created_unix":
		i.Created = time.Unix(i.CreatedUnix, 0).Local()
	case "updated_unix":
		i.Updated = time.Unix(i.UpdatedUnix, 0).Local()
	}
}

func (i *Image) GetInfo() (*Information, error) {
	return GetInformationByHashtag(i.HashTag)
}

func CountImages() int64 {
	return countImages(x)
}

func countImages(e Engine) int64 {
	count, _ := e.Where("1=1").Count(new(Image))
	return count
}

func CountImageHashtag(hashtag string) int64 {
	return countImageHashtag(x, hashtag)
}

func countImageHashtag(e Engine, hashtag string) int64 {
	count, _ := e.Where("hash_tag=?", hashtag).Count(new(Image))
	return count
}

func GetImageByIId(iid string) (*Image, error) {
	t := &Image{IId: iid}
	has, err := x.Get(t)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrImageNotExist{iid}
	}
	return t, nil
}

func IsImageExist(uid int64, iid string) (bool, error) {
	if len(iid) == 0 {
		return false, nil
	}

	return x.Where("id!=?", uid).Get(&Image{IId: iid})
}

func CreateImage(i *Image) error {
	return createImage(x, i)
}

func createImage(e Engine, i *Image) (err error) {
	isExist, err := IsImageExist(0, i.IId)
	if err != nil {
		return err
	} else if isExist {
		return ErrImageNotExist{i.IId}
	}

	_, err = e.Insert(i);
	return err
}

func UpdateImage(i *Image) error {
	return updateImage(x, i)
}

func updateImage(e Engine, i *Image) error {
	_, err := e.Id(i.Id).AllCols().Update(i)
	return err
}

func UpdateImageByIId(IId string, i *Image) error {
	iFound, err := GetImageByIId(IId);
	if err != nil {
		return err
	}

	iFound.Caption = i.Caption
	iFound.DisplaySrc = i.DisplaySrc
	iFound.ThumbnailSrc = i.ThumbnailSrc
	iFound.Likes = i.Likes
	iFound.Height = i.Height
	iFound.Width = i.Width
	iFound.Used = i.Used

	return updateImage(x, i)
}


func Images(page, pageSize int) ([]*Image, error) {
	images := make([]*Image, 0, pageSize)
	return images, x.Limit(pageSize, (page-1)*pageSize).Where("1=1").Desc("id").Find(&images)
}

type SearchImageOptions struct {
	Keyword  string
	OrderBy  string
	Page     int
	PageSize int
}

func SearchImageByHashtag(opts *SearchImageOptions) (images []*Image, _ int64, _ error) {

	if opts.PageSize <= 0 || opts.PageSize > setting.ExplorePagingNum {
		opts.PageSize = setting.ExplorePagingNum
	}

	if opts.Page <= 0 {
		opts.Page = 1
	}

	searchQuery := "%" + opts.Keyword + "%"
	images = make([]*Image, 0, opts.PageSize)

	sess := x.Where("hash_tag LIKE ?", searchQuery)

	var countSess xorm.Session

	countSess = *sess
	count, err := countSess.Count(new(Image))
	if err != nil {
		return nil, 0, fmt.Errorf("Count: %v", err)
	}

	if len(opts.OrderBy) > 0 {
		sess.OrderBy(opts.OrderBy)
	} else {
		sess.OrderBy("id DESC")
	}
	
	log.Info("Fuck %v ", opts.PageSize)
	
	return images, count, sess.Limit(opts.PageSize, (opts.Page-1)*opts.PageSize).Find(&images)
}

