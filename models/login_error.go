package models

import "fmt"

type ErrPasswordInvalid struct {
}

func IsErrPasswordInvalid(err error) bool {
	_, ok := err.(ErrPasswordInvalid)
	return ok
}

func (err ErrPasswordInvalid) Error() string {
	return fmt.Sprint("Password or user invalid")
}
