package models

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"time"

	"github.com/go-xorm/xorm"

	"bitbucket.org/rodkranz/instagram/modules/base"
	"bitbucket.org/rodkranz/instagram/modules/markdown"
	"strings"
)

var (
	ErrNameEmpty    = errors.New("Name is empty")
	ErrTokenExpired = errors.New("The Token is invalid or expired")
)

/**
 * user model
 */
type User struct {
	Id          int64
	
	FullName    string `xorm:"NOT NULL"`
	UserName    string `xorm:"NOT NULL"`
	
	Passwd      string `xorm:"NOT NULL"`
	
	Rands       string `xorm:"VARCHAR(10)"`
	Salt        string `xorm:"VARCHAR(10)"`
	
	Created     time.Time `xorm:"-"`
	CreatedUnix int64
	
	Updated     time.Time `xorm:"-"`
	UpdatedUnix int64
}

/**
 * BeforeInsert execute before insert user
 */
func (u *User) BeforeInsert() {
	u.CreatedUnix = time.Now().UTC().Unix()
	u.UpdatedUnix = u.CreatedUnix
}

/**
 * BeforeUpdate execute before update user
 */
func (u *User) BeforeUpdate() {
	u.UpdatedUnix = time.Now().UTC().Unix()
}

/**
 * EncodePasswd encodes password to safe format.
 */
func (u *User) EncodePasswd() {
	newPasswd := base.PBKDF2([]byte(u.Passwd), []byte(u.Salt), 10000, 50, sha256.New)
	u.Passwd = fmt.Sprintf("%x", newPasswd)
}

/**
 * ValidatePassword checks if given password matches the one belongs to the user.
 */
func (u *User) ValidatePassword(passwd string) bool {
	newUser := &User{Passwd: passwd, Salt: u.Salt}
	newUser.EncodePasswd()
	return u.Passwd == newUser.Passwd
}

/**
 * AfterSet execute after change user column
 */
func (u *User) AfterSet(colName string, _ xorm.Cell) {
	switch colName {
	case "full_name":
		u.FullName = markdown.Sanitizer.Sanitize(u.FullName)
	case "created_unix":
		u.Created = time.Unix(u.CreatedUnix, 0).Local()
	case "updated_unix":
		u.Updated = time.Unix(u.UpdatedUnix, 0).Local()
	}
}


/**
 * GetUserByName returns user by given name.
 */
func GetUserByName(name string) (*User, error) {
	if len(name) == 0 {
		return nil, ErrUserNotExist{0, name}
	}
	
	u := &User{UserName: strings.ToLower(name)}
	has, err := x.Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrUserNotExist{0, name}
	}
	return u, nil
}

/**
 * GetUserByID returns the user object by given ID if exists.
 */
func getUserByID(e Engine, id int64) (*User, error) {
	u := new(User)
	has, err := e.Id(id).Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, ErrUserNotExist{id, ""}
	}
	return u, nil
}
func GetUserByID(id int64) (*User, error) {
	return getUserByID(x, id)
}

/**
 * IsUserExist return if has user with this name or id
 */
func IsUserExist(uid int64, username string) (bool, error) {
	if len(username) == 0 {
		return false, nil
	}
	
	return x.Where("id!=?", uid).Get(&User{UserName: strings.ToLower(username)})
}

/**
 * GetUserSalt returns a ramdom user salt token.
 */
func GetUserSalt() string {
	return base.GetRandomString(10)
}


/**
* CreateUser create new user
 */
func CreateUser(u *User) error {
	return createUser(x, u)
}
func createUser(e Engine, u *User) (err error) {
	isExist, err := IsUserExist(0, u.UserName)
	if err != nil {
		return err
	} else if isExist {
		return ErrUserAlreadyExist{u.UserName}
	}
	
	u.UserName = strings.ToLower(u.UserName)
	u.Rands = GetUserSalt()
	u.Salt = GetUserSalt()
	u.EncodePasswd()
	
	sess := x.NewSession()
	defer sess.Close()
	if err = sess.Begin(); err != nil {
		return err
	}
	
	if _, err = sess.Insert(u); err != nil {
		sess.Rollback()
		return err
	}
	
	return sess.Commit()
}

/**
* UpdateUser update a new user
 */
func UpdateUser(u *User) error {
	return updateUser(x, u)
}
func updateUser(e Engine, u *User) error {
	_, err := e.Id(u.Id).AllCols().Update(u)
	return err
}
