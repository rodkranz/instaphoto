package models

import "fmt"

type ErrInformationNotExist struct {
	HashTag string
}

func IsErrInformationNotExist(err error) bool {
	_, ok := err.(ErrInformationNotExist)
	return ok
}

func (err ErrInformationNotExist) Error() string {
	return fmt.Sprintf("Information not found [hashtag: %s]", err.HashTag)
}
